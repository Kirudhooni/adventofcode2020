//part 1 and part 2

// read file and get puzzle input
var fs = require("fs");
var puzzleInput = fs.readFileSync("./input.txt" , "utf-8").split('\n');
var doneTwo = false;
var doneThree = false;
//add each number by all others and see if they sum to 2020
puzzleInput.forEach(element => {
    let val = parseInt(element)
    for(let i = 0; i <puzzleInput.length; i++ ){
        if((val + parseInt(puzzleInput[i])) == 2020 && !doneTwo) {
            console.log('multiplication of two numbers = ' + (val * parseInt(puzzleInput[i])));       //get the multiple
            doneTwo = true;
        }
        if((val + parseInt(puzzleInput[i])) < 2020){
            let sumTwo = val + parseInt(puzzleInput[i]);
            for(let j = 0; j <puzzleInput.length; j++ ){
                if((sumTwo + parseInt(puzzleInput[j])) == 2020 && !doneThree) {
                    console.log('multiplication of three numbers = ' + (val * parseInt(puzzleInput[i]) * parseInt(puzzleInput[j])));       //get the multiple
                    doneThree = true;
                }
            }
        }
    }
});

console.log("done!");
