//part 1 and part 2

//part1
var fs = require('fs');
var puzzleInput = fs.readFileSync('./input.txt', 'utf-8').split('\n');
var counter = 0;
lineIndex = 0;
var numTrees = 0;
puzzleInput.forEach(line=>{
    if(counter > 0){
        if(line[lineIndex] == '#') numTrees++;
    }
    lineIndex +=3;
    if(lineIndex>30)lineIndex = (lineIndex % 30) -1;
    counter++;
});
console.log('Part 1 number of trees = '+ numTrees);

//part 2
counter = 0;
lineIndex = 0;
numTrees = 0;
horizontalDistance = [1, 3, 5, 7, 1];
verticalDistance = [1, 1, 1, 1, 2];
let treeTotal = [0,0,0,0,0];

for(let i = 0; i<horizontalDistance.length; i++){
    for(let j = 0; j<puzzleInput.length; j=j+verticalDistance[i]){
        line = puzzleInput[j];
        if(counter > 0){
           if(line[lineIndex] == '#') numTrees++;         
        }
        lineIndex +=horizontalDistance[i];
        if(lineIndex>30)lineIndex = (lineIndex % 30) -1;
        counter++;
    }
    treeTotal[i] = numTrees;
    numTrees = 0;
    counter=0;
    lineIndex=0;
}
console.log('Part 2 number of trees = '+ treeTotal.reduce( ( prod, elem ) => prod* elem ));