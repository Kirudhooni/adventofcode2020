//part 1 and part 2



var fs = require('fs');
//read from file
var puzzleInput = fs.readFileSync('./input.txt', 'utf-8').split('\n');
var validPasswords = 0;
//iterate check procedure for each password
puzzleInput.forEach(line => {
    let splitLine = line.split(' ');
    let limit = splitLine[0];
    let lowerLimit = limit.split('-')[0];
    let upperLimit = limit.split('-')[1];
    let letter = splitLine[1][0];
    let password = splitLine[2];
    let counter = 0;

    for (let i = 0; i<password.length; i++){
        if(password[i] == letter) counter++;
    }
    if ((counter >= parseInt(lowerLimit)) && (counter <= parseInt(upperLimit))) validPasswords++;
    
});
console.log('Part 1 valid password count = ' + validPasswords);


//part 2
validPasswords = 0;
puzzleInput.forEach(line => {
    let splitLine = line.split(' ');
    let limit = splitLine[0];
    let positionOne = parseInt(limit.split('-')[0]) -1;
    let positionTwo = parseInt(limit.split('-')[1]) -1;

    let letter = splitLine[1][0];
    let password = splitLine[2];

    if(((password[positionOne] == letter) && (password[positionTwo] != letter)) || ((password[positionOne] != letter) && (password[positionTwo] == letter)) )validPasswords++;
    
});

console.log('Part 2 valid password count = ' + validPasswords);
console.log('done!');