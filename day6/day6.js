//part 1 and part 2
var fs = require('fs');
var puzzleInput = fs.readFileSync('./input.txt', 'utf-8').split('\r\n');
var sumGroups = 0;


//part 1
let currentString ='';
 
for(let i = 0; i<puzzleInput.length;i++){
    
    if(puzzleInput[i] == '' || i == puzzleInput.length-1){
        let uniqLetters = '';
        currentString = currentString.concat(puzzleInput[i]);
        for(let j = 0; j<currentString.length;j++){
            if(uniqLetters.indexOf(currentString.charAt(j)) == -1) uniqLetters += currentString[j];
        }
        sumGroups += uniqLetters.length;
        currentString ='';
    }
    else currentString = currentString.concat(puzzleInput[i]);
    

}
console.log(sumGroups);

//part 2
sumGroups = 0;
var group =[]
var commonNumber =0;
for(let i = 0; i<puzzleInput.length;i++){
    
    if(puzzleInput[i] == '' || i == puzzleInput.length-1){
        if(i == puzzleInput.length-1)group.push(puzzleInput[i]);
        for(let j = 0; j<group[0].length; j++){
            commonNumber = 0;
            group.forEach(person=>{
                if(person.includes(group[0][j]))commonNumber++;
            });
            if(commonNumber == group.length) sumGroups += 1;    
        }
        group =[];
    }else{
        group.push(puzzleInput[i]);
    }
}

console.log(sumGroups)