//part 1 and part 2

var fs = require('fs');
var puzzleInput = fs.readFileSync('./input.txt', 'utf-8').split('\r\n');
var boardingCode = new Array();
puzzleInput.forEach(code=>{
    var x = [0,128];
    var y = [0,8];
    var row;
    var column;
    var diff;
    for(let i = 0 ; i< code.length; i++){
        let letter = code[i];
        
        if(letter == 'F'){
            diff = Math.abs(x[1]-x[0]);
            x[0] = x[0];
            x[1] = x[0]+(diff/2);
            
        }
        if(letter == 'B'){
            diff = Math.abs(x[1]-x[0]);
            x[0] = x[1]-(diff/2);
            x[1] = x[1];
            
        }

        if(i == 6){
            if(letter == 'F') row  = x[0];
            else row = x[1]-1;
        }
        if(letter == 'R'){
            diff = Math.abs(y[1]-y[0]);
            y[0] = y[1]-(diff/2);
            y[1] = y[1];
           
        }
        if(letter == 'L'){
            diff = Math.abs(y[1]-y[0]);
            y[0] = y[0];
            y[1] = y[0]+(diff/2);
           
        }
        if(i == 9){
            if(letter == 'L') column  = y[0];
            else column = y[1]-1;
            boardingCode.push(row* 8 + column);
        }

    }

    
});
console.log('Max code: ' +Math.max.apply(null,boardingCode));



//part 2
var missingCodes = new Array();

for(let i = 0; i<Math.max.apply(null,boardingCode)+1; i++){
    if(!(boardingCode.includes(i)))missingCodes.push(i);
}
var myCode;
for(let i = 0; i<missingCodes.length-1; i++){
    if(Math.abs(missingCodes[i+1] - missingCodes[i]) > 1)myCode = missingCodes[i+1];
}
console.log('My code: ' + myCode);
console.log('done!');