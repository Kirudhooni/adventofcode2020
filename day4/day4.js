//part 1 and part 2


//part 1
var fs = require('fs');
const { connected } = require('process');
puzzleInput = fs.readFileSync('./input.txt', 'utf-8').split("\r\n")
var counter = 0;
var validPassport = 0;
var validPassportTwo = 0; //for part 2
var validCounter = 0;
var validCounterTwo = 0;    //for part 2
puzzleInput.forEach(element => {
    counter++;
    lineBreakDown = element.split(' ');   
    lineBreakDown.forEach(combo=>{
        characteristics = combo.split(':');
        let key =  characteristics[0];
        let value = characteristics[1];
        switch (key){
            case 'byr':{
                validCounter++;
                if(parseInt(value) >= 1920 && parseInt(value) <= 2002 ) validCounterTwo ++;
                break;
            }
            case 'iyr':{
                validCounter++;
                if(parseInt(value) >= 2010 && parseInt(value) <= 2020 ) validCounterTwo ++;
                break;
            }
            case 'eyr':{
                validCounter++;
                if(parseInt(value) >= 2020 && parseInt(value) <= 2030 ) validCounterTwo ++;
                break;
            }
            case 'hgt':{
                validCounter++;
                if(value.includes('cm')){
                    if(parseInt(value.split('c')[0]) >= 150 && parseInt(value.split('c')[0]) <= 193) validCounterTwo++;
                }else{
                    if(parseInt(value.split('i')[0]) >= 59 && parseInt(value.split('i')[0]) <= 76) validCounterTwo++;
                }
                break;
            }
            case 'hcl':{
                validCounter++;
                if(value.match(/#[0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f]/g) != null) validCounterTwo++;
                break;
            }
            case 'ecl':{
                validCounter++;
                if(/amb|blu|brn|gry|grn|hzl|oth/.test(value)) validCounterTwo ++;
                break;
            }
            case 'pid':{
                validCounter++;
                if(value.length == 9 && (value.match(/[0-9]+/g) != null)) validCounterTwo++;
                break;
            }
        }
    });
    if (element == '' || counter == puzzleInput.length){
        if (validCounter == 7)validPassport++;
        if (validCounterTwo == 7)validPassportTwo++;
        validCounter = 0;
        validCounterTwo = 0;
    }
});
console.log('Part 1 number of valid passports: ' + validPassport);
console.log('Part 2 number of valid passports: ' + validPassportTwo);
console.log('done!')

